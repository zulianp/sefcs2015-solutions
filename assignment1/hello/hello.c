#include <stdio.h>

/**
 * To compile:
 * gcc -Wall -std=c99 -o hello_world hello.c
 * To run: 
 * ./hello_world
 * Or just: source compile_and_run.sh
 */

int main(int argc, char * argv[])
{
	printf("Hello, World!\n");
	return 0;
}
