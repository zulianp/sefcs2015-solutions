################## Git initialization ######################
CONF_TYPE='--local'
#CONF_TYPE='--gobal'

git config $CONF_TYPE user.name "Patrick Zulian"
git config $CONF_TYPE user.email "patrick.zulian@usi.ch"
git config $CONF_TYPE color.status auto
git config $CONF_TYPE color.branch auto
git config $CONF_TYPE push.default tracking
git config $CONF_TYPE core.editor nano
git config $CONF_TYPE core.excludefile .gitignore_local

# Make it feel a bit more like SVN.
git config $CONF_TYPE alias.co checkout
git config $CONF_TYPE alias.st status
git config $CONF_TYPE alias.br branch
git config $CONF_TYPE alias.up pull
git config $CONF_TYPE alias.his \
'log --pretty=format:\"%h %ad | %s%d [%an]\" --graph --date=short'

################## Custom prompt ################## 

#Import all colors.
source .colors_def

#Color scopes begin.
#PROMPT_COLOR=$Cyan'\]'
PROMPT_COLOR=$Red'\]'

#Color scope end
RESET_COLOR='\[\e[0m\]'

#Here the two shell variables are overriden.
export PS1=$PROMPT_COLOR'[\u@\h \W] \$'$RESET_COLOR 
export PS2="more:"

################## Aliases ################################## 
alias l='ls -lahGp'
