#ifndef STENCIL_HPP
#define STENCIL_HPP

#include <array>
#include <vector>
#include <functional>
#include <algorithm>
#include <numeric>



namespace sefcs {
	//Forward declaration
	template<int Dim> 
	class Grid;

	template<int Dim>
	class Stencil {};

	template<>
	class Stencil<2> {
	public:
		constexpr static int n_dimensions() { return 2; }

		Stencil(std::initializer_list<int> i_index, 
				std::initializer_list<int> j_index, 
				std::initializer_list<double> weights, const double scale_factor = 1)
		: scale_factor_(scale_factor)
		{
			using std::copy;
			using std::begin;
			using std::end;
		

			assert(i_index.size() == weights.size());
			assert(j_index.size() == i_index.size());



			index_[0].resize(i_index.size());
			index_[1].resize(j_index.size());
			weights_.resize(weights.size());

			copy( begin(i_index), end(i_index), begin(index_[0]) );
			copy( begin(j_index), end(j_index), begin(index_[1]) );
			copy( begin(weights), end(weights), begin(weights_) );

			init();
		}

		void apply(const long i, const long j, const Grid<2> &in, Grid<2> &out) const
		{
			long n_i = index_[0].size();
			
			double val = 0;
			for(long k = 0; k < n_i; ++k) {
				val += in(i + index_[0][k], 
					      j + index_[1][k]) * weights_[k];
			}

			out(i, j) = scale_factor_ * val;
		}

		inline int begin(const int dim) const { return begin_[dim]; }
		inline int end(const int dim) const { return end_[dim]; }
	private:
		std::array<int,  2> begin_, end_;
		std::array< std::vector<int>, 2> index_;
		std::vector<double> weights_;
		double scale_factor_;

		void init()
		{
			using namespace std;

			for(int i = 0; i < 2; ++i) {
				begin_[i] = accumulate( std::begin(index_[i]), 
									    std::end(index_[i]),
									    numeric_limits<int>::max(), 
									    [] (int left, int right) ->int { return min(left, right); }
									);
				end_[i]   = accumulate( std::begin(index_[i]), 
										std::end(index_[i]), 
										numeric_limits<int>::lowest(),  
										[] (int left, int right) ->int { return max(left, right); });
			}
		}
	};

	template<class Stencil>
	void apply(const Stencil &stencil, const Grid<2> &in, Grid<2> &out)
	{
		assert(&in != &out);

		const long begin_i = -stencil.begin(0);
		const long begin_j = -stencil.begin(1);

		const long end_i = in.size(0) - stencil.end(0);
		const long end_j = in.size(1) - stencil.end(1);

		for(long i = begin_i; i != end_i; ++i) {
			for(long j = begin_j; j != end_j; ++j) {
				stencil.apply(i, j, in, out);
			}
		}
	}
}

#endif //STENCIL_HPP