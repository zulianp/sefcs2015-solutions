#include "Grid.hpp"
#include "Stencil.hpp"

namespace sefcs {

	template<class Grid, class Stencil>
	void explicit_euler(const Grid &initial_value, const Stencil &stencil, const long n_timesteps, const double delta_t, Grid &result)
	{
		using std::transform;
		using std::begin;
		using std::end;
		
		//copy size, boundary values....
		Grid update = initial_value;
		result = initial_value;

		for(long t = 0; t < n_timesteps; ++t) {
			apply(stencil, result, update);
			transform(
				begin(result.data()), end(result.data()), 
				begin(update.data()), begin(result.data()), 
				[delta_t](const double x, const double delta_x) -> double {
					return x - delta_t*delta_x;
				}
				);

			write_to_file(result, t+1, n_timesteps);
		}
	}

	template<class Grid>
	void init_grid(const long n_x, const long n_y, Grid &grid)
	{
		grid.resize(n_x, n_y);
		std::fill(std::begin(grid.data()), std::end(grid.data()), 1);

		for(long i = 0; i < n_x; ++i) {
			grid(i, 0) = 0;
			grid(i, n_y-1) = 0;
		}

		for(long j = 0; j < n_y; ++j) {
			grid(0, j) = 0;
			grid(n_x-1, j) = 0;
		}
	}
}

int main(int argc, char * argv[])
{
	using namespace sefcs;

	long n_x = 9, n_y = 8;
	long n_timesteps = 100;
	double diffusion_constant = 1;

	if(argc >= 3) {
		n_x = std::stol(argv[1]);
		n_y = std::stol(argv[2]);
		if(n_x < 3 || n_y < 3) {
			std::cerr << "Wrong input" << std::endl;
			return EXIT_FAILURE;
		}

		if(argc > 3) {
			n_timesteps = std::stol(argv[3]);

			if(argc > 4) {
				diffusion_constant = std::atof(argv[4]);
			}
		}
	}

	//4D usage
	// Grid<4> grid4;
	// grid4.resize(4, 4, 4, 4);
	// std::fill(std::begin(grid4.data()), std::end(grid4.data()), 0);
	// grid4(0, 1, 2, 3) = 1;
	// std::cout << grid4(0, 0, 0, 0) << std::endl;

	Grid<2> grid;
	init_grid(n_x, n_y, grid);

	std::cout << "------------------------- Input ----------------------------" << std::endl;
	describe(grid, std::cout);

	Grid<2> result; 
	//stencil := (x, y, weight)
	Stencil<2> laplacian({ -1, 1,  0,  0, 0}, {0, 0, 1, -1, 0}, {-1, -1, -1, -1, 4});

	explicit_euler(grid, laplacian, n_timesteps, 10./n_timesteps, result);

	std::cout << "------------------------- Result ----------------------------" << std::endl;
	describe(result, std::cout);
	return EXIT_SUCCESS;
}

