#ifndef GRID_HPP
#define GRID_HPP

#include <iostream>     // std::cout
#include <iterator>     // std::ostream_iterator
#include <vector>       // std::vector
#include <algorithm>    // std::copy
#include <array>
#include <assert.h>

#include <fstream>
#include <cmath>
#include <string>

namespace sefcs {
	typedef double Scalar;

	typedef struct {
		template<int Dim, typename Integer, typename ...Args>
		inline static long get_index(const long * sizes, const Integer i, Args... args)
		{
			static_assert(Dim == sizeof...(Args)+1, "Wrong sizes provided to get_index.");

			assert(i < sizes[0]);
			assert(i >= 0);
			return i + sizes[0] * get_index<Dim-1>(&sizes[1], args...);
		}

		template<int Dim, typename Integer>
		inline static long get_index(const long * sizes, const Integer i) {
			(void) sizes;
			assert(i < sizes[0]);
			assert(i >= 0);
			return i;
		}

		template<int Dim, typename ...Args>
		inline static void set_size(long * sizes, const long size, Args... args)
		{
			static_assert(Dim == sizeof...(Args)+1, "Wrong sizes provided to get_index.");

			sizes[0] = size;
			set_size<Dim-1>(&sizes[1], args...);
		}

		template<int Dim>
		inline static void set_size(long * sizes, const long size)
		{
			sizes[0] =  size;
		}

	} TensorAccess;

	template<int Dim>
	class Grid {
	public:
		template<typename ...Args>
		void resize(Args...args)
		{
			static_assert(sizeof...(Args) == Dim, "number of arguments must be equal to Dim");

			TensorAccess::set_size<Dim>(&sizes_[0], args...);
			long storage_size = sizes_[0];
			for(int d = 1; d < Dim; ++d) {
				storage_size *= sizes_[d];
			}

			data_.resize(storage_size);
		}

		template<typename ...Args>
		Scalar &operator()(Args...args) 
		{
			assert(TensorAccess::get_index<Dim>(&sizes_[0], args...) < static_cast<long>(data_.size()));
			return data_[TensorAccess::get_index<Dim>(&sizes_[0], args...)];
		}

		template<typename ...Args>
		inline const Scalar &operator()(Args...args) const
		{
			assert(TensorAccess::get_index<Dim>(&sizes_[0], args...) < static_cast<long>(data_.size()));
			return data_[TensorAccess::get_index<Dim>(&sizes_[0], args...)];
		}

		void describe(std::ostream &os) const
		{
			os << "sizes\n";
			for(int d = 0; d < Dim; ++d) {
				os << " " << sizes_[d];
			}

			os << "\n";

			std::ostream_iterator<Scalar> out_it (os,", ");
  			std::copy ( data_.begin(), data_.end(), out_it );
		}

		inline std::vector<Scalar> &data()
		{
			return data_;
		}

		inline long size(const int dim) const
		{
			assert(dim < Dim);
			assert(dim >= 0);
			return sizes_[dim];
		}


	private:
		std::array<long, Dim> sizes_;
		std::vector<Scalar> data_;
	};

	//use standard of specialize
	template<int Dim>
	void describe(const Grid<Dim> &grid, std::ostream &os)
	{
		grid.describe(os);
	}

	void describe(const Grid<2> &grid, std::ostream &os)
	{
		for(long i = 0; i < grid.size(0); ++i) {
			for(long j = 0; j < grid.size(1); ++j) {
				os << grid(i, j) << "\t";
			}

			os << "\n";
		}
	}

	void write_to_file(const Grid<2> &grid, const std::string &path)
	{
		std::ofstream os(path);
		if(!os.good()) {
			os.close();
			return;
		}

		describe(grid, os);

		os.close();
	}

	void write_to_file(const Grid<2> &grid, const long step, const long n_timesteps)
	{
		using std::log10;
		const long max =     ceil(log10(1 + n_timesteps));
		const long current = ceil(log10(1 + step));

		std::string zeros;
		zeros.assign(max-current, '0');

		write_to_file(grid, "sim_"+ zeros  + std::to_string(step) + ".txt");
	}
}

#endif //GRID_HPP
