#ifndef PROFILER_HPP
#define PROFILER_HPP 

#include "Monitor.hpp"
#include <memory>
#include <vector>
#include <map>

namespace sefcs {

	//This implementation is very memory inefficient.
	class Profiler : public Monitor {
	public:
		
		//Inner class: explain...
		class Invocation {
		public:
			Invocation(const std::string &name);
			~Invocation();
			void clear();

			void add_child(const std::shared_ptr<Invocation> &child);
			void set_parent(const std::shared_ptr<Invocation> &parent);
			std::shared_ptr<Invocation> get_parent();
			std::vector< std::shared_ptr<Invocation> > &get_children();


			const std::string &get_name() const;
			long get_ticks() const;
			void set_ticks(const long ticks);
		private:
			std::string name_;
			long ticks_;
			std::vector< std::shared_ptr<Invocation> > children_;
			std::shared_ptr<Invocation> parent_;
		};

		class Aggregation {
		public:
			Aggregation()
			: min_(std::numeric_limits<long>::max()), max_(0), total_(0), count_(0)
			{}

			long min_;
			long max_;
			long total_;
			long count_;
		};


		void method_begin(const std::string &name) override;
		void method_end(const std::string &name) override;
		void monitoring_ended() override;

	private:
		void visit_print(const std::shared_ptr<Invocation> &invocation, const int level, std::ostream &os);
		void visit_collect(const std::shared_ptr<Invocation> &invocation, 
						   std::map<std::string, std::shared_ptr<Aggregation> > &aggregates);
		static float ticks_to_seconds(const long ticks);

		std::shared_ptr<Invocation> tree_;
		std::vector< std::shared_ptr<Invocation> > forest_;
	};
}

#endif //PROFILER_HPP
