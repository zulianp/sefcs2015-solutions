
#include "App.hpp"
#include "Example.hpp"
#include "CallingContextTreeMonitor.hpp"
#include "Profiler.hpp"

#include <stdlib.h>
#include <iostream>

int main(const int argc, const char * argv[])
{	
	using namespace sefcs;

	//make_shared and shared_ptr what are they for??
	using std::make_shared;

	if(argc > 1) {
		//check the argv for input
		const std::string arg1 = argv[1];
		if(arg1 == "-prof") {
			App::instance().set_monitor(make_shared<Profiler>());
		} else if(arg1 == "-cct") {
			App::instance().set_monitor(make_shared<CallingContextTreeMonitor>());
		}
	} else {
		std::cout << "No monitor defined using default: CallingContextTreeMonitor " << std::endl;
		//default monitor
		App::instance().set_monitor(make_shared<CallingContextTreeMonitor>());
	}


	Example example;
	example.method_a();
	example.method_d();
	
	std::vector<std::string> v(1, "HI");

	//Finalizing the app 
	App::instance().finalize();
	return EXIT_SUCCESS;
}


