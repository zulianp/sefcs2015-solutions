#include "Profiler.hpp"

#include <assert.h>
#include <iostream>
#include <time.h> 

namespace sefcs {
	//For convenience we define a local type for the inner class
	typedef Profiler::Invocation InvocationT;
	typedef Profiler::Aggregation AggregationT;

	InvocationT::Invocation(const std::string &name)
	: name_(name), ticks_(0), parent_(nullptr) { }

	InvocationT::~Invocation()
	{
		// printf("~Invocation\n");
	}

	void InvocationT::clear()
	{
		for(auto child_ptr : children_) {
			child_ptr->clear();
		}

		children_.clear();
	}

	void InvocationT::add_child(const std::shared_ptr<InvocationT> &child) {
		children_.push_back(child);
	}

	void InvocationT::set_parent(const std::shared_ptr<InvocationT> &parent) {
		parent_ = parent;
	}

	std::shared_ptr<InvocationT> InvocationT::get_parent() {
		return parent_;
	}

	std::vector< std::shared_ptr<InvocationT> > & InvocationT::get_children()
	{
		return children_;
	}

	const std::string & InvocationT::get_name() const
	{
		return name_;
	}

	long InvocationT::get_ticks() const
	{
		return ticks_;
	}

	void InvocationT::set_ticks(const long ticks)
	{
		ticks_ = ticks;
	}

	void Profiler::method_begin(const std::string &name) {
		auto node = std::make_shared<InvocationT>(name);
		if(tree_) {
			tree_->add_child(node);
			node->set_parent(tree_);
			tree_ = node;
		} else {
			tree_ = node;
		}

		clock_t t;
		tree_->set_ticks(clock());
	}

	void Profiler::method_end(const std::string &name) {
		assert(bool(tree_) && "tree_ should never be null!");
		(void) name; //unused
	
		if(!tree_) {
			std::cerr << "[Error] Profiler::method_end, this->tree_ is null" << std::endl;
			return;
		}
		tree_->set_ticks(clock() - tree_->get_ticks());

		if(!tree_->get_parent()) {
			forest_.push_back(tree_);
		}

		tree_ = tree_->get_parent();
	}


	void Profiler::monitoring_ended() {
		using namespace std;

		ostream &os = std::cout;

		if(forest_.empty()) {
			os << "Empty\n";
			return;
		}

		for(auto tree_ptr : forest_) {
			visit_print(tree_ptr, 0, os);
		}	

		map<string, shared_ptr<AggregationT> > aggregates;

		for(auto tree_ptr : forest_) {
			visit_collect(tree_ptr, aggregates);
		}	

		long total = 0;

		for(auto pair : aggregates) {
			os << "---------------------------------------------------\n"; 
			os << pair.first << ":\n";
			os << "count: " << pair.second->count_ << "\n";
			os << "total: " << ticks_to_seconds(pair.second->total_) << "\n";
			os << "min:   " << ticks_to_seconds(pair.second->min_) << "\n";
			os << "max:   " << ticks_to_seconds(pair.second->max_) << "\n";
			os << "---------------------------------------------------\n";

			total += pair.second->total_;
		}

		os << "total time: " << ticks_to_seconds(total) << "\n";

		//clean-up after ourselves
		for(auto tree_ptr : forest_) {
			tree_ptr->clear();
		}

		forest_.clear();
	}

	void Profiler::visit_collect(const std::shared_ptr<InvocationT> &invocation, 
					   std::map<std::string, std::shared_ptr<AggregationT> > &aggregates)
	{
		using namespace std;

		if(!invocation) return;

		auto &aggregate_ptr = aggregates[invocation->get_name()];
		if(!aggregate_ptr) {
			aggregate_ptr = make_shared<AggregationT>();
		}

		aggregate_ptr->count_++; 
		aggregate_ptr->total_ += invocation->get_ticks();
		aggregate_ptr->min_ = min(invocation->get_ticks(), aggregate_ptr->min_);
		aggregate_ptr->max_ = max(invocation->get_ticks(), aggregate_ptr->max_);

		for(auto child_ptr : invocation->get_children()) {
			visit_collect(child_ptr, aggregates);
		}
	}

	void Profiler::visit_print(const std::shared_ptr<InvocationT> &invocation, const int level, std::ostream &os)
	{

		if(!invocation) return;
		

		for(int i = 0; i < level; ++i) {
			os << "     ";
		}

		if(level > 0) {
			os << "|--> ";
		}

		os << invocation->get_name() << " time: (" << ticks_to_seconds(invocation->get_ticks()) << " seconds)\n";

		//depth first traversal 
		for(auto child_ptr : invocation->get_children()) {
			visit_print(child_ptr, level+1, os);
		}
	}

	float Profiler::ticks_to_seconds(const long ticks) {
		return (((float)ticks)/CLOCKS_PER_SEC);
	}
}



