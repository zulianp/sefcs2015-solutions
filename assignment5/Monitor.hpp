#ifndef MONITOR_HPP
#define MONITOR_HPP 

#include <string>

namespace sefcs {
	class Monitor {
	public:
		virtual ~Monitor() {}
		/** Override this for intercepting the beginning of a method call
		 * @param name is the name (or signature) of the method being called	
		 */
		 virtual void method_begin(const std::string &name) = 0;

		/** Override this for intercepting the end of a method call
		 * @param name is the name (or signature) of the method being called	
		 */
		 virtual void method_end(const std::string &name) = 0;

		/** 
		 * Override this to produce an output from your monitoring
		 */
		 virtual void monitoring_ended() = 0;
		};	
	}

#endif
