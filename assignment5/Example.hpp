#ifndef EXAMPLE_HPP
#define EXAMPLE_HPP

namespace sefcs {
	class Example {
	public:
		//Some methods
		void method_a();
		void method_b();
		void method_c();
		void method_d();
	};
}

#endif //EXAMPLE_HPP
