#include "App.hpp"
#include "CallingContextTreeMonitor.hpp"

namespace sefcs {
	App &App::instance()
	{
		static App instance_;
		return instance_;
	}

	App::App()
	: monitor_(std::make_shared<CallingContextTreeMonitor>())
	{}


	void App::finalize()
	{
		monitor()->monitoring_ended();
	}
}
