# Software Engineering for Computational Science
## Solutions for the assignments

September 16 - December 16, 2015.

### Links
Course page: https://bitbucket.org/psanan/sefcs2015

### Assignments
For each assignment in the course page there is a folder with the reference solution.
