

#include <stdio.h>
#include <stdlib.h>


/**
 * To compile and run: gcc -std=c99 lorenz.c -o runme && ./runme
 */
int main()
{
	//Input values
	static const int K = 36;
	const double F = -1;
	const double dt = 0.0001;

	//Storage
	double X[K];
	double dX[K];

	//Initial condititon
	for(int k = 0; k < K; ++k) {
		X[k] = ((2345 * k) % 10000)/10000.0;	
	}

	const int T = 1000;
	for(int t = 0; t < T;  ++t) {
		//Compute update
		for(int k = 0; k < K; ++k) {
			//stencil indices
			const int k_plus_K = k + K;
			const int k_minus_2 = (k_plus_K - 2) % K;
			const int k_minus_1 = (k_plus_K - 1) % K; 
			const int k_plus_1  = (k_plus_K + 1) % K; 

			dX[k] = -X[k_minus_2] * X[k_minus_1] + X[k_minus_1] * X[k_plus_1] - X[k] + F;
		}	

		//Integration step (explicit Euler) + energy computation
		double energy = 0;
		for(int k = 0; k < K; ++k) {
			X[k] += dt * dX[k];
			energy += X[k] * X[k];
		}

		energy *= 0.5;

		printf("Step: %3.0d, Energy: %g\n", t+1, energy);

		for(int k = 0; k < K; ++k) {
			//Warning this resolution might not be ideal if the data needs to be used 
			printf("%.4f ", X[k]);
		}

		printf("\n");
	}
	
	return 0;
}
