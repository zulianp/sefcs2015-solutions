# Assignment 2: Solution

## 1) Warmup: Debugging practice 
See annotations inside fixed.c. Test it with valgrind.

## 2) Exploring the Lorenz 99 System 
To compile and run type: 
	gcc -std=c99 lorenz.c -o runme && ./runme

## 3) Unix plumbing
To run the script type:
	source run_lorenz.sh 