#ifndef EXPRESSION_H
#define EXPRESSION_H 

namespace sefcs {
	class Expression {
	public:
		virtual double value() const = 0;
		virtual ~Expression() {}
	};


	class Value : public Expression {
	public:
		inline double value() const 
		{
			return value_;
		}

		inline Value(const double value) 
		: value_(value) {}

	private:
		double value_;
	};
}

#endif //EXPRESSION_H