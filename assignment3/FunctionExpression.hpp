#ifndef FUNCTION_EXPRESSION_HPP
#define FUNCTION_EXPRESSION_HPP 

#include "BinaryExpression.hpp"
#include <iostream>
#include <string>
#include <memory>
#include <cmath>

namespace sefcs {

	class FunctionExpression : public Expression {
	public:
		bool make(std::istream &is);

		double value() const override;

	private:
		std::string function_name_;
		BinaryExpression expr_;
	};
}

#endif //FUNCTION_EXPRESSION_HPP
