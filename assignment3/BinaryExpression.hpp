#ifndef BINARY_EXPRESSION_HPP
#define BINARY_EXPRESSION_HPP

#include "Expression.hpp"

#include <functional> 
#include <map>
#include <string>
#include <memory>


namespace sefcs {

	class BinaryExpression : public Expression {
	public:
		typedef std::function<double(double, double)> Function;

		BinaryExpression(const std::shared_ptr<Expression> &left = nullptr, const std::shared_ptr<Expression> right = nullptr, const std::string &operator_name = "");


		double interpret_literal(std::istream &is, const char delimiter = '\n');
		void make(std::istream &is, const char delimiter);
		void usage();
	
		Function get_function(const std::string &operator_name) const;
		
		inline Function get_function() const
		{
			return get_function(operator_name_);
		}
		
		inline double value() const override {
			return get_function(operator_name_)(left_->value(), right_->value());
		}

		inline bool is_function(const std::string &operator_name) const {
			return funcs_.find(operator_name) != funcs_.end();
		}

	private:
		static double add(double l, double r);
		static double subtract(double l, double r);
		static double multiply(double l, double r);
		static double divide(double l, double r);
		static double null(double l, double r);

		void init();


		std::map<std::string, Function> funcs_;
		std::shared_ptr<Expression> left_, right_;
		std::string operator_name_;
		
	};
}

#endif //BINARY_EXPRESSION_HPP
