
#include "BinaryExpression.hpp"

#include <iostream>
#include <stdio.h>
#include <assert.h>

namespace sefcs {

	double BinaryExpression::interpret_literal(std::istream &is, const char delimiter)
	{
		using namespace std;
	
		//read amd compute
		string line; 
		getline(is, line, delimiter);

		double left, right;
		char temp[10];

		sscanf(line.c_str(), "%lg %c %lg", &left, temp, &right);
		string op_string(temp);

		auto it = funcs_.find(op_string);
		if(it == funcs_.end()) {
			std::cerr << "[Error] operation " << op_string << " not supported" << std::endl;
			return 0;
		}

		const double result = it->second(left, right);
		return result;
	}

	void BinaryExpression::make(std::istream &is, const char delimiter)
	{
		using namespace std;
	
		//read amd compute
		string line; 
		getline(is, line, delimiter);

		double left, right;
		char temp[10];

		sscanf(line.c_str(), "%lg %c %lg", &left, temp, &right);
		string op_string(temp);

		auto it = funcs_.find(op_string);
		if(it == funcs_.end()) {
			std::cerr << "[Error] in " << line << " operation " << op_string << " not supported" << std::endl;
			assert(false);
		}

		left_ = std::make_shared<Value>(left);
		right_ = std::make_shared<Value>(right);
		operator_name_ = op_string;		
	}


	BinaryExpression::BinaryExpression(const std::shared_ptr<Expression> &left, const std::shared_ptr<Expression> right, const std::string &operator_name)
	: left_(left), right_(right), operator_name_(operator_name)
	{
		init();
	}

	void BinaryExpression::usage()
	{
		using namespace std;

		cout << "type in <number><operation><number>" << std::endl;
		cout << "available operations are: ";
		
		for(auto fun : funcs_) {
			cout << fun.first << " ";
		}

		cout << endl;
	}

	BinaryExpression::Function BinaryExpression::get_function(const std::string &operator_name) const
		{
			auto it = funcs_.find(operator_name);
			if(it == funcs_.end()) return null;
			return it->second;
		}

	double BinaryExpression::add(double l, double r) { return l+r; }
	double BinaryExpression::subtract(double l, double r) { return l-r; }
	double BinaryExpression::multiply(double l, double r) { return l*r; }
	double BinaryExpression::divide(double l, double r) { return l/r; }
    double BinaryExpression::null(double l, double r) { 
    	(void) l;
    	(void) r;
    	return 0; 
    }

	void BinaryExpression::init()
	{
		funcs_["+"] = add;
		funcs_["-"] = subtract;
		funcs_["*"] = multiply;
		funcs_["/"] = divide;
	}
}
