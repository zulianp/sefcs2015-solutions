#include <iostream>
#include <map>
#include <string>
#include <functional> 
#include <stdio.h>


#include "FunctionExpression.hpp"


//to run it: use make run, make test or for instance ./main
//you can write abs(<binary_expression>) or exp(<binary_expression>)
//type exit() to exit
int main(int argc, char * argv[])
{
	using namespace std;
	
	(void) argc;
	(void) argv;

	while(cin.good()) {
		sefcs::FunctionExpression op;
		if(!op.make(cin)) break;

		cout << op.value() << endl;
	}
	return 0;
}
