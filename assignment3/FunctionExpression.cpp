
#include "FunctionExpression.hpp"
#include <iostream>
#include <string>
#include <memory>
#include <cmath>
#include <sstream> 

namespace sefcs {

	bool FunctionExpression::make(std::istream &is)
	{
		using namespace std;

		string line;
		getline(is, line, '\n');
		if(line.empty()) {
			std::cout << "empty line: exiting.." << std::endl;
			return false;
		}

		size_t pos = line.find_first_of('(');
		
		//no function
		if(string::npos == pos) {
			line.append("\n");
			istringstream iss(line);
			expr_.make(iss, '\n');
			return true;
		}

		//function
		function_name_ = line.substr(0, pos);
		if(function_name_ == "exit") {
			std::cout << "exiting.." << std::endl;
			return false;
		}

		size_t last_pos = line.find_first_of(')');

		string expr = line.substr(pos+1, last_pos+1);
		istringstream iss(expr);

		expr_.make(iss, ')');
		return true;
	}

	double FunctionExpression::value() const 
	{
		if(function_name_ == "abs") {
			return std::abs(expr_.value());
		} else if(function_name_ == "exp") {
			return exp(expr_.value());
		}

		return expr_.value();
	}	
}

